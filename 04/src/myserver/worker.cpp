// Copyright 2013 15418 Course Staff.

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sstream>
#include <glog/logging.h>

#include "server/messages.h"
#include "server/worker.h"
#include "tools/work_queue.h"

using namespace std;

void *threadFunction(void* work_queue_arg);
// Thread Safe Work Queue for each worker
static WorkQueue<Request_msg> work_queue;

void* threadFunction(void*){

    while(true){

	Request_msg req = work_queue.get_work();
  	Response_msg resp(req.get_tag());

        // actually perform the work.  The response string is filled in by
        // 'execute_work'

        execute_work(req, resp);


  	// send a response string to the master
  	worker_send_response(resp);
    }	
    return 0;
}


void worker_node_init(const Request_msg& params) {

  // This is your chance to initialize your worker.  For example, you
  // might initialize a few data structures, or maybe even spawn a few
  // pthreads here.  Remember, when running on Amazon servers, worker
  // processes will run on an instance with a dual-core CPU.
  
  pthread_t thread1;
  pthread_t thread2;
   
  printf("**** Initializing worker Threads: %s ****\n", params.get_arg("name").c_str());
  if (pthread_create(&thread1, NULL, &threadFunction, NULL) != 0) {
    	printf("Thread Create Error!\n");
  }
  printf(" Thread 1 Created \n"); 
  if (pthread_create(&thread2, NULL, &threadFunction, NULL) != 0) {
    	printf("Thread Create Error!\n");
  }
  printf(" Thread 2 Created\n ");
}

void worker_handle_request(const Request_msg& req1) {


  // Make the tag of the reponse match the tag of the request.  This
  // is a way for your master to match worker responses to requests.
  work_queue.put_work(req1);

 }
