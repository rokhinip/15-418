// Copyright 2013 Harry Q. Bovik (hbovik)
#include <assert.h>
#include <glog/logging.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>

#include "server/messages.h"
#include "server/master.h"

using namespace std;
 
typedef map<int, Client_handle> Waiting_clients; 
typedef map<Worker_handle, pair<int, bool> > Worker_load;
typedef map<string, string> Cache;
typedef map<int, string> Request_cache;

struct Pending_request {

  Request_msg *client_req;
  Pending_request *next_req; 

};

struct Pending_requests {

  Pending_request *head;
  Pending_request *tail;

};

static struct Master_state {

  bool server_ready;

  /***** Client information *****/
  // Map from tag to client_handle. 
  Waiting_clients waiting_clients; 
  // Linked list of pending requests. If list is NULL, then no pending requests
  Pending_requests *pending_requests; 
  int num_pending_requests;

  /***** Work information *****/
  int max_num_workers; // Size of total worker pool. This is fixed
  int max_work_per_worker;
  // Map from worker_handle to <workload, has_IO_intensive_job>
  Worker_load worker_load;
  
  /****** Performance *******/
  int incoming_requests_within_tick;
  int serviced_requests_within_tick;

  /****** Cache *******/
  // Cache is currently unbounded
  Cache cache;          // Maps request string to response string
  Request_cache request_cache;  // Maps tag to request string

  /******* Compare Primes *******/
  map<int, map<int, int> > compare_prime_parent_to_child;
  map<int, pair<int,int> > compare_prime_child_to_parent_position; 
} mstate;

/* Interface for working with worker_load map */

Worker_handle get_free_worker(bool check_IO_intensive){

  Worker_handle worker_handle = NULL;
  int min_workload = mstate.max_work_per_worker;

  map<Worker_handle, pair<int, bool> >::iterator it;
  for (it = mstate.worker_load.begin(); it != mstate.worker_load.end(); it++){

    int work_load = it->second.first;
    bool has_IO_intensive_job = it->second.second;

    if (work_load < min_workload) {

      if ((check_IO_intensive && !has_IO_intensive_job) || !check_IO_intensive) {
        min_workload = work_load;
        worker_handle = it->first;
      }

    }
  }
    
  if (worker_handle == NULL || 
      (mstate.worker_load[worker_handle].first >= mstate.max_work_per_worker)) {
    // Work of the minimally busy worker is too much. So no free workers
    return NULL;
  } else {
    return worker_handle;
  }

}

void print_queue () {
  Pending_request *current = mstate.pending_requests->head;
  while (current != NULL) {
    printf("Request String: %s  --> ", 
    current->client_req->get_request_string().c_str());
    current = current->next_req;
  }

  printf("NULL\n");
}


/* Interface for working with pending requests queue */

Pending_request *get_pending_request(){
  if (mstate.pending_requests->head == NULL){
    // Removing from an empty queue
    return NULL;

  } else {

    Pending_request *request = mstate.pending_requests->head;
    mstate.pending_requests->head = mstate.pending_requests->head->next_req;

    // Implies we removed from singleton queue
    if (mstate.pending_requests->head == NULL) {  
      mstate.pending_requests->tail = NULL;
    }

    mstate.num_pending_requests--;
    return request;
  }
}

void insert_pending_request(Request_msg *client_req){

  Pending_request *pending_request = 
    (Pending_request *) malloc(sizeof(struct Pending_request));

  pending_request->client_req = client_req;
  pending_request->next_req = NULL;
  
  if (mstate.pending_requests->head == NULL &&
      mstate.pending_requests->tail == NULL) { 
    // Inserting into an empty queue
    mstate.pending_requests->head = pending_request;
    mstate.pending_requests->tail = pending_request;
  } else {
    // Inserting into a non-empty queue
    mstate.pending_requests->tail->next_req = pending_request;
    mstate.pending_requests->tail = pending_request;
  }

  mstate.num_pending_requests++;
}

/* Initialize master node */

void master_node_init(int max_workers, int &tick_period) {

  // set up tick handler to fire every 5 seconds. (feel free to
  // configure as you please)
  tick_period = 5;

  mstate.server_ready = false;

  mstate.max_num_workers = max_workers;
  mstate.max_work_per_worker = 2;

  mstate.pending_requests = 
    (Pending_requests *) malloc(sizeof(struct Pending_requests));
  mstate.pending_requests->head = NULL;
  mstate.pending_requests->tail = NULL;

  mstate.num_pending_requests = 0;
  mstate.incoming_requests_within_tick = 0;
  mstate.serviced_requests_within_tick = 0;

  // fire off 1 worker
  int tag = random();

  Request_msg *req = new Request_msg(tag);
  req->set_arg("name", "my worker");
  request_new_worker_node(*req);
  printf("Fired off 1 worker!\n");

}
        
// Takes in a worker who is free but not in free queue, either assigns work or 
// inserts into free queue
void make_busy_worker(Worker_handle worker_handle){

  // If there is work left to do and this worker can afford to do more work,
  // then send the work to the worker
  if (mstate.num_pending_requests > 0 && 
      mstate.worker_load[worker_handle].first < mstate.max_work_per_worker){

    Pending_request *new_req = get_pending_request();

    // Cache the request string
    int tag = new_req->client_req->get_tag();
    string request_string = new_req->client_req->get_request_string();
    mstate.request_cache[tag] = request_string;

    send_request_to_worker(worker_handle, *(new_req->client_req));

    mstate.worker_load[worker_handle].first++;

  } 

}

void handle_new_worker_online(Worker_handle worker_handle, int tag) {

  // 'tag' allows you to identify which worker request this response
  // corresponds to.  Since the starter code only sends off one new
  // worker request, we don't use it here.

  tag = tag; // to get compiler to shut up

  mstate.worker_load[worker_handle] = make_pair(0, false); 

  if (mstate.server_ready == false) { // In the beginning when setting up master
    server_init_complete();
    mstate.server_ready = true;
  } else {
    make_busy_worker(worker_handle);
  }

  return;

}


void handle_compare_prime_child_response(int main_child_tag,const Response_msg& resp){
	
	// Cache result
  	mstate.cache[mstate.request_cache[main_child_tag]] = resp.get_response();
     
        pair<int,int> parent_position_pair = mstate.compare_prime_child_to_parent_position[main_child_tag];
	int parent_tag = parent_position_pair.first;
	mstate.compare_prime_parent_to_child[parent_tag][main_child_tag] = atoi(resp.get_response().c_str());
        map<int, int> parent_map = mstate.compare_prime_parent_to_child[parent_tag];
	
	int count = 0;	
	int resp_int[4];
        for (std::map<int,int>::iterator it=parent_map.begin(); it!=parent_map.end(); ++it){
   		int child_tag = it->first;
		int value = it->second;
 		if (value==-1)
		    break;
		else {
		    count++;
		    pair<int,int> parent_pair = mstate.compare_prime_child_to_parent_position[child_tag]; 
		    resp_int[parent_pair.second] = value;;
		}
	} 
	Response_msg resp_temp(0);	
	if(count!=4){
	    // If there is work left to do, send it to this possibly free worker
   	    return;			
	}
	else {
	    //printf(" Done with all 4 TASKS !!\n");
	    if (resp_int[1]-resp_int[0] > resp_int[3]-resp_int[2])
      		resp_temp.set_response("There are more primes in first range.");
    	    else
      		resp_temp.set_response("There are more primes in second range.");   	
	}
        int tag = parent_tag;
	 
    	// Send response to client
  	Waiting_clients::iterator it = mstate.waiting_clients.find(tag);
  	Client_handle client_handle = it->second;

  	mstate.waiting_clients.erase(it); // The client is no longer waiting
  	send_client_response(client_handle, resp_temp);

	return;
}

void handle_worker_response(Worker_handle worker_handle, const Response_msg& resp) {

  // Show reduced workload
  mstate.serviced_requests_within_tick++;
  mstate.worker_load[worker_handle].first--;

  // IO intensive work done!
  int tag = resp.get_tag(); 
  if (mstate.request_cache[tag].find("mostviewed") != std::string::npos) {
    mstate.worker_load[worker_handle].second = false;
  }
  
  if(mstate.compare_prime_child_to_parent_position.count(tag) > 0){
	handle_compare_prime_child_response(tag,resp);
  	// If there is work left to do, send it to this possibly free worker
  	make_busy_worker(worker_handle);
	return;
  }
	 
  // Cache result
  mstate.cache[mstate.request_cache[tag]] = resp.get_response();
   
  // Send response to client
  Waiting_clients::iterator it = mstate.waiting_clients.find(tag);
  Client_handle client_handle = it->second;

  mstate.waiting_clients.erase(it); // The client is no longer waiting
  send_client_response(client_handle, resp);

  // If there is work left to do, send it to this possibly free worker
  make_busy_worker(worker_handle);
  return;

}


// Generate a valid 'countprimes' request dictionary from integer 'n'
static void create_computeprimes_req(Request_msg& req, int n) {
  std::ostringstream oss;
  oss << n;
  req.set_arg("cmd", "countprimes");
  req.set_arg("n", oss.str());
}


void handle_client_request(Client_handle client_handle, const Request_msg& client_req) {

  // You can assume that traces end with this special message.  It
  // exists because it might be useful for debugging to dump
  // information about the entire run here: statistics, etc.
  if (client_req.get_arg("cmd") == "lastrequest") {
    Response_msg resp(0);
    resp.set_response("ack");
    send_client_response(client_handle, resp);
    return;
  }
  
  // Should never happen
  if (mstate.max_num_workers == 0){
    Response_msg resp(0);
    resp.set_response("Cannot handle request");
    send_client_response(client_handle, resp);
    return;
  }

  // Request is in the cache
  string request_string = client_req.get_request_string();
  if (mstate.cache.count(request_string)) {
    Response_msg resp(0);
    resp.set_response(mstate.cache[request_string]);
    send_client_response(client_handle, resp);
    return;

  }

  // Request is not in cache, need to compute it
  mstate.incoming_requests_within_tick++;

  int tag = random();
  mstate.waiting_clients[tag] = client_handle;
  mstate.request_cache[tag] = request_string;
  Request_msg *worker_req = new Request_msg(tag, client_req);  

  bool check_IO_intensive = (client_req.get_arg("cmd") == "mostviewed");
  bool check_compare_prime = (client_req.get_arg("cmd")=="compareprimes");
  
  if(check_compare_prime){
    
        int params[4];
	
    	params[0] = atoi(client_req.get_arg("n1").c_str());
    	params[1] = atoi(client_req.get_arg("n2").c_str());
    	params[2] = atoi(client_req.get_arg("n3").c_str());
    	params[3] = atoi(client_req.get_arg("n4").c_str());

	// n0 > n2 and (n1-n0) > (n3-n2)
	//   or
	// n2 > n0 and (n3-n2) > (n1-n0)
       
  
        for (int i=0; i<4; i++) {
 	    int child_tag = random();
            
  	    // Creating the Individual Requests
            Request_msg *worker_req = new Request_msg(child_tag);
      	    create_computeprimes_req(*worker_req, params[i]);
            
            // Populate Appropriate Key-Value Maps
            mstate.compare_prime_parent_to_child[tag][child_tag] = -1;
  	    mstate.compare_prime_child_to_parent_position[child_tag] = make_pair(tag,i);
            
  	    Worker_handle worker_handle = get_free_worker(false);

	     // Request is in the cache
  	    string request_string = (*worker_req).get_request_string();
	    printf(" Looking for string : %s\n",request_string.c_str());
            if (mstate.cache.count(request_string)) {
		printf(" Cache Hit for Compute prime in Compare \n");
    		Response_msg resp(0);		 
   		resp.set_response(mstate.cache[request_string]);
    		handle_compare_prime_child_response(child_tag,resp);
                mstate.worker_load[worker_handle].first--;	
		//return;
  	    	continue;
	    }

      
   	    if(worker_handle!=NULL){
		mstate.request_cache[child_tag] = request_string;
		mstate.worker_load[worker_handle].first++;
    		send_request_to_worker(worker_handle, *worker_req);
 	    }
	    else{
   		insert_pending_request(worker_req);
	    }
    	}
	return;
  }

  Worker_handle worker_handle = get_free_worker(check_IO_intensive);

  if (check_IO_intensive && worker_handle == NULL) {
      
      worker_handle = get_free_worker(!check_IO_intensive);

      if (worker_handle != NULL) { 
        // Found a worker who could take it even if he has IO intensive job
        
        assert(mstate.worker_load[worker_handle].second == true);
        mstate.worker_load[worker_handle].first++;
        send_request_to_worker(worker_handle, *worker_req);

      } else { // Still couldn't find a worker

        insert_pending_request(worker_req);

      } 

  } else if (check_IO_intensive && worker_handle != NULL) {

    assert(mstate.worker_load[worker_handle].second == false);
    mstate.worker_load[worker_handle].second = true;
    mstate.worker_load[worker_handle].first++;
    send_request_to_worker(worker_handle, *worker_req);

  } else if (!check_IO_intensive && worker_handle == NULL){

    insert_pending_request(worker_req);

  } else { // !check_IO_intensive && worker_handle != NULL

    mstate.worker_load[worker_handle].first++;
    send_request_to_worker(worker_handle, *worker_req);

  }

  printf("Pending request queue size: %d\n", mstate.num_pending_requests);
  if (mstate.num_pending_requests > 2){
  /*
      int tag = random();

      Request_msg *req = new Request_msg(tag);
      req->set_arg("name", "my worker");

      request_new_worker_node(*req);
      printf("fired off a new worker!\n");
   */
  } 
  return;

}


void handle_tick() {

  int incoming = mstate.incoming_requests_within_tick;
  int serviced = mstate.serviced_requests_within_tick;

  if (incoming > serviced) {
    // we have a backlog of requests
    
    int new_workers_needed = (incoming - serviced)/mstate.max_work_per_worker;
    int num_busy_workers = 0; 

    // Calculate the number of workers with work in work queue
    map<Worker_handle, pair<int,bool> >::iterator it;
    for (it = mstate.worker_load.begin(); it != mstate.worker_load.end(); it++){
      int work_load = it->second.first;

      if (work_load != 0) num_busy_workers++;
    }

    new_workers_needed = min(mstate.max_num_workers - num_busy_workers,
                             new_workers_needed);

    for (int i = 0; i < new_workers_needed; i++){
      int tag = random();

      Request_msg *req = new Request_msg(tag);
      req->set_arg("name", "my worker");

      request_new_worker_node(*req);
      printf("fired off a new worker!\n");

    }

  } else {
    // we have idle workers
    
    int no_workers_to_kill = (serviced - incoming)/mstate.max_work_per_worker;
    int total_running_workers = mstate.worker_load.size();

    if (no_workers_to_kill >= total_running_workers) {
      no_workers_to_kill = total_running_workers - 1; // Leave 1 worker up
    }
   
    
    map<Worker_handle, pair<int,bool> >::iterator it;
    for (it = mstate.worker_load.begin(); it != mstate.worker_load.end(); it++){

      if (no_workers_to_kill == 0) return;
     
      Worker_handle worker_handle = it->first;
      int work_load = it->second.first;

      if (work_load == 0){
        kill_worker_node(worker_handle);
        mstate.worker_load.erase(mstate.worker_load.find(worker_handle));

        no_workers_to_kill--;
        printf("killed worker!\n");
      }

    }

  }

  // to reset for next tick
  mstate.incoming_requests_within_tick = 0;
  mstate.serviced_requests_within_tick = 0;

}

