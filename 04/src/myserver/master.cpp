// Copyright 2013 Harry Q. Bovik (hbovik)
#include <assert.h>
#include <glog/logging.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>

#include "server/messages.h"
#include "server/master.h"
#include "tools/cycle_timer.h"

using namespace std;
 
typedef map<int, Client_handle> Waiting_clients; 
typedef map<Worker_handle, pair<int, bool> > Worker_load;
typedef map<string, string> Cache;
typedef map<int, string> Request_cache;
typedef pair<int, int> Parent_and_position;
typedef map<int, int> Child_tag_to_value;
typedef map<int, pair<double, double> > Latency_measure;

struct Pending_request {

  Request_msg *client_req;
  Pending_request *next_req; 

};

struct Pending_requests {

  Pending_request *head;
  Pending_request *tail;

};

static struct Master_state {

  bool server_ready;

  /***** Client information *****/
  // Map from tag to client_handle. 
  Waiting_clients waiting_clients; 
  // Linked list of pending requests. If list is NULL, then no pending requests
  Pending_requests *pending_requests; 
  int num_pending_requests;

  /***** Work information *****/
  int max_num_workers; // Size of total worker pool. This is fixed
  int max_work_per_worker;
  // Map from worker_handle to <workload, has_IO_intensive_job>
  Worker_load worker_load;
  
  /****** Performance *******/
  Latency_measure latency_measure;
  int incoming_requests_within_tick;
  int serviced_requests_within_tick;

  /****** Cache *******/
  // Cache is currently unbounded
  Cache cache;          // Maps request string to response string
  Request_cache request_cache;  // Maps tag to request string

  /******* Compare Primes *******/
  map<int, Child_tag_to_value > compare_prime_parent_to_child;
  map<int, Parent_and_position > compare_prime_child_to_parent_position;
 
  // Count for How many primes from compare prime have a cache miss 
  int check_compareprime_flag;
} mstate;

/************ Interface for working with worker_load map *************/
Worker_handle get_free_worker(bool check_IO_intensive){

  Worker_handle worker_handle = NULL;
  int min_workload = mstate.max_work_per_worker;

  map<Worker_handle, pair<int, bool> >::iterator it;
  for (it = mstate.worker_load.begin(); it != mstate.worker_load.end(); it++){

    int work_load = it->second.first;
    bool has_IO_intensive_job = it->second.second;

    if (work_load < min_workload) {

      if ((check_IO_intensive && !has_IO_intensive_job) || !check_IO_intensive) {
        min_workload = work_load;
        worker_handle = it->first;
      }

    }
  }
    
  if (worker_handle == NULL || 
      (mstate.worker_load[worker_handle].first >= mstate.max_work_per_worker)) {
    // Work of the minimally busy worker is too much. So no free workers
    return NULL;
  } else {
    return worker_handle;
  }

}

/************* Interface for working with pending requests queue **************/

Pending_request *get_pending_request(){
  if (mstate.pending_requests->head == NULL){
    // Removing from an empty queue
    return NULL;

  } else {

    Pending_request *request = mstate.pending_requests->head;
    mstate.pending_requests->head = mstate.pending_requests->head->next_req;

    // Implies we removed from singleton queue
    if (mstate.pending_requests->head == NULL) {  
      mstate.pending_requests->tail = NULL;
    }

    mstate.num_pending_requests--;
    return request;
  }
}

void insert_pending_request(Request_msg *client_req){

  Pending_request *pending_request = 
    (Pending_request *) malloc(sizeof(struct Pending_request));

  pending_request->client_req = client_req;
  pending_request->next_req = NULL;
  
  if (mstate.pending_requests->head == NULL &&
      mstate.pending_requests->tail == NULL) { 
    // Inserting into an empty queue
    mstate.pending_requests->head = pending_request;
    mstate.pending_requests->tail = pending_request;
  } else {
    // Inserting into a non-empty queue
    mstate.pending_requests->tail->next_req = pending_request;
    mstate.pending_requests->tail = pending_request;
  }

  mstate.num_pending_requests++;
}

/**************** Initialize master node ******************/

void master_node_init(int max_workers, int &tick_period) {

  tick_period = 5;

  mstate.server_ready = false;

  mstate.max_num_workers = max_workers;
  mstate.max_work_per_worker = 2;

  mstate.pending_requests = 
    (Pending_requests *) malloc(sizeof(struct Pending_requests));
  mstate.pending_requests->head = NULL;
  mstate.pending_requests->tail = NULL;
  mstate.num_pending_requests = 0;

  mstate.incoming_requests_within_tick = 0;
  mstate.serviced_requests_within_tick = 0;

  int tag = random();

  for (int i=0;i < mstate.max_num_workers/4;i++){
     Request_msg *req = new Request_msg(tag);
     req->set_arg("name", "my worker");
     request_new_worker_node(*req);
  }
  Request_msg *req = new Request_msg(tag);
  req->set_arg("name", "my worker");
  request_new_worker_node(*req);

}
  
void send_work_to_worker(Worker_handle worker_handle, const Request_msg& req){

    mstate.worker_load[worker_handle].first++;
    if (req.get_arg("cmd") == "mostviewed") { // Set true for IO intensive
      mstate.worker_load[worker_handle].second = true;
    }
    // Record start time of request 
    double start_time = CycleTimer::currentSeconds();
    mstate.latency_measure[req.get_tag()] = make_pair(start_time, -1);

    send_request_to_worker(worker_handle, req);

}
        
// Takes in a worker who is free but not in free queue, either assigns work or 
// inserts into free queue
void make_busy_worker(Worker_handle worker_handle){

  // If there is work left to do and this worker can afford to do more work,
  // then send the work to the worker
  if (mstate.num_pending_requests > 0 && 
      mstate.worker_load[worker_handle].first < mstate.max_work_per_worker){

    Pending_request *new_req = get_pending_request();

    // Cache the request string
    int tag = new_req->client_req->get_tag();
    string request_string = new_req->client_req->get_request_string();
    mstate.request_cache[tag] = request_string;


    //Send the job to the worker
    send_work_to_worker(worker_handle, *(new_req->client_req));

  } 

}

void handle_new_worker_online(Worker_handle worker_handle, int tag) {

  // 'tag' allows you to identify which worker request this response
  // corresponds to.  Since the starter code only sends off one new
  // worker request, we don't use it here.

  tag = tag; // to get compiler to shut up

  mstate.worker_load[worker_handle] = make_pair(0, false); 

  if (mstate.server_ready == false) { // In the beginning when setting up master
    server_init_complete();
    mstate.server_ready = true;
  } else {
    make_busy_worker(worker_handle);
  }

  return;

}


void handle_compare_prime_child_response(int child_tag, const Response_msg& resp){
    
    // Cache result
    mstate.cache[mstate.request_cache[child_tag]] = resp.get_response();

    // We received response from child. Now find the parent request and 
    // the actual position in orig request this response corresponds to    	
    Parent_and_position parent_position_pair = 
      mstate.compare_prime_child_to_parent_position[child_tag];
    int parent_tag = parent_position_pair.first;
    int computed_primality = atoi(resp.get_response().c_str());

    // Set the child value
    mstate.compare_prime_parent_to_child[parent_tag][child_tag] = computed_primality;

    // Now we want to see if we can respond to the request corresponding to
    // parent tag
    Child_tag_to_value child_tag_to_value = 
      mstate.compare_prime_parent_to_child[parent_tag];
    
    int resp_int[4];
    Child_tag_to_value::iterator it;  
    
    // We can't proceed if the map hasn't been populated with 4 elements.
    if (child_tag_to_value.size() < 4){
	return;
    }
    
    for (it = child_tag_to_value.begin(); it != child_tag_to_value.end(); ++it) {
      int possible_child_tag = it->first;
      int possible_child_value = it->second;
      if (possible_child_value == -1) {
        // We cannot respond to parent request since we don't have enough
        // information
        return;
      } else {
        Parent_and_position parent_and_pos = 
          mstate.compare_prime_child_to_parent_position[possible_child_tag]; 
        assert(( 0 <= parent_and_pos.second) &&  ( parent_and_pos.second < 4));
        resp_int[parent_and_pos.second] = possible_child_value;
      }
    } 
   
    // If we get here, it means that we have all the 4 required numbers
    Response_msg main_resp(0);	

    if (resp_int[1]-resp_int[0] > resp_int[3]-resp_int[2]) {
        main_resp.set_response("There are more primes in first range.");
    } else {
        main_resp.set_response("There are more primes in second range.");
    }

    // Send response to client
    Waiting_clients::iterator clientIt = mstate.waiting_clients.find(parent_tag);
    Client_handle client_handle = clientIt->second;

    mstate.waiting_clients.erase(clientIt); // The client is no longer waiting
    send_client_response(client_handle, main_resp);

    return;
}


void handle_worker_response(Worker_handle worker_handle, const Response_msg& resp) {


  int tag = resp.get_tag(); 
  // Measure end time of job
  mstate.latency_measure[tag].second = CycleTimer::currentSeconds();

  // Reduced workload for worker
  mstate.worker_load[worker_handle].first--;
  mstate.serviced_requests_within_tick++;
  // IO intensive work done!
  // TODO: This may not necessarily be right esp if a worker has multiple IO
  // intensive jobs
  if (mstate.request_cache[tag].find("mostviewed") != std::string::npos) {
    mstate.worker_load[worker_handle].second = false;
  }
 
  // If this response is to a count_prime request for compare_primes
  if (mstate.compare_prime_child_to_parent_position.count(tag) > 0) {

    handle_compare_prime_child_response(tag, resp);
    
  } else { 
    // This response is for a regular request, not about compare_primes
    
    // Cache result
    mstate.cache[mstate.request_cache[tag]] = resp.get_response();
   
    // Send response to client
    Waiting_clients::iterator it = mstate.waiting_clients.find(tag);
    Client_handle client_handle = it->second;

    mstate.waiting_clients.erase(it); // The client is no longer waiting
    send_client_response(client_handle, resp);
  }

  // If there is work left to do, send it to this possibly free worker
  make_busy_worker(worker_handle);
  return;

}


// Generate a valid 'countprimes' request dictionary from integer 'n'
static void create_computeprimes_req(Request_msg& req, int n) {
  std::ostringstream oss;
  oss << n;
  req.set_arg("cmd", "countprimes");
  req.set_arg("n", oss.str());
}

void handle_compare_primes_client_request(const Request_msg& client_req, 
                                          int parent_tag){
  int params[4];
  
  params[0] = atoi(client_req.get_arg("n1").c_str());
  params[1] = atoi(client_req.get_arg("n2").c_str());
  params[2] = atoi(client_req.get_arg("n3").c_str());
  params[3] = atoi(client_req.get_arg("n4").c_str());
   

  for (int i = 0; i < 4; i++) {

    int child_tag = random();
    
    // Creating the Individual Requests
    Request_msg *worker_req = new Request_msg(child_tag);
    create_computeprimes_req(*worker_req, params[i]);
    
    // Populate Appropriate Key-Value Maps
    mstate.compare_prime_parent_to_child[parent_tag][child_tag] = -1;
    mstate.compare_prime_child_to_parent_position[child_tag] = 
      make_pair(parent_tag,i);
   
    // compute prime request is in the cache
    string request_string = worker_req->get_request_string();
    mstate.request_cache[child_tag] = request_string;
        
    
    if (mstate.cache.count(request_string)) {
         
        Response_msg resp(0);		 
        resp.set_response(mstate.cache[request_string]);
	// We may Call this Function with the child_tag_to_value map 
	// might have less than 4 Elements.
        handle_compare_prime_child_response(child_tag, resp);

    } else { 
      // This element is not a cache hit. Thus  
      mstate.check_compareprime_flag++;
      // compute prime request is not in cache, need to actually execute work
      mstate.incoming_requests_within_tick++;
      // compare_primes is not an IO intensive job 
      Worker_handle worker_handle = get_free_worker(false);

      if (worker_handle != NULL){
          send_work_to_worker(worker_handle, *worker_req);
      } else {
          insert_pending_request(worker_req);
      }
    }
  }

  return;
}

void handle_client_request(Client_handle client_handle, const Request_msg& client_req) {

  // You can assume that traces end with this special message. It
  // exists because it might be useful for debugging to dump
  // information about the entire run here: statistics, etc.
  if (client_req.get_arg("cmd") == "lastrequest") {
    Response_msg resp(0);
    resp.set_response("ack");
    send_client_response(client_handle, resp);
    return;
  }
  
  // Should never happen
  if (mstate.max_num_workers == 0){
    Response_msg resp(0);
    resp.set_response("Cannot handle request");
    send_client_response(client_handle, resp);
    return;
  }

  // Wholesale request is in the cache
  string request_string = client_req.get_request_string();
  if (mstate.cache.count(request_string)) {
    Response_msg resp(0);
    resp.set_response(mstate.cache[request_string]);
    send_client_response(client_handle, resp);
    return;

  }

  // Request is not in cache, need to compute it
  int tag = random();
  mstate.waiting_clients[tag] = client_handle;
  mstate.request_cache[tag] = request_string;
  Request_msg *worker_req = new Request_msg(tag, client_req);  

  bool check_IO_intensive = (client_req.get_arg("cmd") == "mostviewed");
  bool check_compare_prime = (client_req.get_arg("cmd")=="compareprimes");
 
  // If request is a compare prime request 
  if (check_compare_prime) {
    handle_compare_primes_client_request(client_req, tag);
    return;
  } 
 
  mstate.incoming_requests_within_tick++;
  // We increment count by 1 for non compare primes requests since execute_work
  // is called only once

  Worker_handle worker_handle = get_free_worker(check_IO_intensive);
  // IO intensive request but no IO free worker
  if (check_IO_intensive && worker_handle == NULL) {
      
      worker_handle = get_free_worker(!check_IO_intensive);

      if (worker_handle != NULL) { 
        // Found a worker who could take it even if he has IO intensive job
        
        assert(mstate.worker_load[worker_handle].second == true);
        send_work_to_worker(worker_handle, *worker_req);

      } else { // Still couldn't find a worker

        insert_pending_request(worker_req);

      } 

  // IO intensive request
  } else if (check_IO_intensive && worker_handle != NULL) {

    assert(mstate.worker_load[worker_handle].second == false);
    send_work_to_worker(worker_handle, *worker_req);

  // Other regular requests
  } else if (!check_IO_intensive && worker_handle == NULL){

    insert_pending_request(worker_req);

  } else { // !check_IO_intensive && worker_handle != NULL

    send_work_to_worker(worker_handle, *worker_req);

  }
  return;
}


void handle_tick() {

  double total_time_taken_by_work = 0;
  int total_work_completed = 0;
  Latency_measure::iterator latencyIt;
  for (latencyIt = mstate.latency_measure.begin(); 
       latencyIt != mstate.latency_measure.end(); ++latencyIt){

    pair<double, double> time = latencyIt->second;
    if (time.second != -1){
      total_time_taken_by_work += (time.second - time.first);
      total_work_completed++;
    }
  }

  double average_latency = total_time_taken_by_work / total_work_completed;

  int incoming = mstate.incoming_requests_within_tick;
  int serviced = mstate.serviced_requests_within_tick;


  if (average_latency >= 2.5 || 
      mstate.num_pending_requests > mstate.max_work_per_worker ||
      incoming > serviced) {
    // We have a backlog of requests
    int new_workers_needed = 1; // Only bring up 1 worker at a time.
    int num_busy_workers = 0; 

    // Calculate the number of workers with work in work queue
    map<Worker_handle, pair<int,bool> >::iterator it;
    for (it = mstate.worker_load.begin(); it != mstate.worker_load.end(); ++it){
      int work_load = it->second.first;

      if (work_load != 0) num_busy_workers++;
    }

    new_workers_needed = min(mstate.max_num_workers - num_busy_workers,
                             new_workers_needed);

    if (new_workers_needed > 0){

      int tag = random();

      Request_msg *req = new Request_msg(tag);
      req->set_arg("name", "my worker");

      request_new_worker_node(*req);

    }
  }
  if ((mstate.check_compareprime_flag < 8) &&  (incoming <= serviced || !mstate.num_pending_requests)) { 

    // we have idle workers 
    int no_workers_to_kill = 1;
    int total_running_workers = mstate.worker_load.size();

    if (no_workers_to_kill >= total_running_workers) {
      no_workers_to_kill = total_running_workers - 1; // Leave 1 worker up
    }
   
    map<Worker_handle, pair<int,bool> >::iterator it;
    for (it = mstate.worker_load.begin(); it != mstate.worker_load.end(); ++it){

      if (no_workers_to_kill == 0) return;
     
      Worker_handle worker_handle = it->first;
      int work_load = it->second.first;

      if (work_load == 0){
        kill_worker_node(worker_handle);
        mstate.worker_load.erase(mstate.worker_load.find(worker_handle));

        no_workers_to_kill--;
      }

    }

  }

  // to reset for next tick
  for (latencyIt = mstate.latency_measure.begin(); 
       latencyIt != mstate.latency_measure.end();) {
   
    pair< double, double> time = latencyIt->second; 
    if (time.second != -1){
      mstate.latency_measure.erase(latencyIt++);
    } else {
      ++latencyIt;
    }
  }

  mstate.incoming_requests_within_tick = 0;
  mstate.serviced_requests_within_tick = 0;
}
