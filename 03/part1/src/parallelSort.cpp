/* Copyright 2014 15418 Staff */

#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <mpi.h>

#include "parallelSort.h"
#include "stlSort.h"
using namespace std;

void printArr(const char* arrName, int *arr, size_t size, int procId) {
#ifndef NO_DEBUG
  for(size_t i=0; i<size; i+=4) {
    printf("%s[%d:%d] on processor %d = %d %d %d %d\n", arrName, i,
        min(i+3,size-1), procId, arr[i], (i+1 < size) ? arr[i+1] : 0, 
        (i+2 < size) ? arr[i+2] : 0, (i+3 < size) ? arr[i+3] : 0); 
  }
#endif
}

void printArr(const char* arrName, float *arr, size_t size, int procId) {
#ifndef NO_DEBUG
  if (size == 0){
    printf("%s is empty", arrName);
  }
  for(size_t i=0; i<size; i+=4) {
    printf("%s[%d:%d] on processor %d = %f %f %f %f\n", arrName, i,
        min(i+3,size-1), procId, arr[i], (i+1 < size) ? arr[i+1] : 0, 
        (i+2 < size) ? arr[i+2] : 0, (i+3 < size) ? arr[i+3] : 0); 
  }
#endif
}

void randomSample(float *data, size_t dataSize, float *sample, size_t sampleSize) {
  for (size_t i = 0; i < sampleSize; i++) {
    sample[i] = data[rand() % dataSize];
  }
}

void findPivots(float *data, int procs, int procId, size_t dataSize, 
                size_t localSize, int numPivots, float *pivots) {

  // Get samples from each local chunk 
  float *allSamples;
  size_t sampleSizePerProc = 12 * log(dataSize);   // As given in handout
  int allSamplesSize = sampleSizePerProc * procs;
  
  if (procId == ROOT) {
    try {
      allSamples = (float *) malloc(sizeof(float) * allSamplesSize);
    } catch (bad_alloc&) {
      printf("Number of samples %d is too large to fit on the machine\n", 
      allSamplesSize);
      return;
    }
  }

  float *samplesPerProc = (float*) malloc(sizeof(float) * sampleSizePerProc);

  randomSample(data, localSize, samplesPerProc, sampleSizePerProc);

  // Send local samples to p0
  assert(MPI_Gather(samplesPerProc, sampleSizePerProc, MPI_FLOAT,
    allSamples, sampleSizePerProc, MPI_FLOAT, ROOT,
    MPI_COMM_WORLD) == MPI_SUCCESS);  


  if (procId == ROOT){

    // Sequentially sort samples
    sort(allSamples, allSamples + allSamplesSize);

    // Pick pivots
    for (int i = 1; i <= numPivots; i++) {

      int sampleLoc = i * sampleSizePerProc;
      float possiblePivot = allSamples[sampleLoc];

      while (((i == 0 && possiblePivot == 0.f) ||  // Ensure non-zero pivots
              (i > 0 && pivots[i-1] == possiblePivot)) &&  // Unique pivots
              sampleLoc < allSamplesSize) {
        sampleLoc++;
        possiblePivot = allSamples[sampleLoc];

      }

      pivots[i-1] = possiblePivot;

    }

  }

  // Send pivot buffer to all processes from ROOT
  MPI_Bcast(pivots, numPivots, MPI_FLOAT, ROOT, MPI_COMM_WORLD);

}

void redistributeElements(float *data, int localSize, float *sendBuf, float *pivots, 
int numPivots, int *sendSizes, int *receiveSizes, int *sendDisps, int *recvDisps,int proc) {

  // Optimization: 
  // We are redistributing the elements from the first half of data and 
  // then doing the same for the elements from the second half of data. This is
  // because localSize is very big and we are working with 3 arrays of localSize
  // during this phase. There is a high probability that these 3 arrays of
  // localSize will not all fit into the cache. Thus we process them in chunks
  // in order to reduce thrashing the cache.
  
  int numBuckets = numPivots + 1;
  int *dataToBucket = (int *) malloc(sizeof(int) * localSize);
 
  int *sendSizesTempLeft =  (int*) calloc(sizeof(int) * numBuckets, sizeof(int));
  int *sendSizesTempRight = (int*) calloc(sizeof(int) * numBuckets, sizeof(int));

  // Determine which bucket each element goes into
  for (int i = 0; i < localSize/2; i++){ 
    int bucketNo = lower_bound(pivots, pivots + numPivots, data[i]) - pivots;
    dataToBucket[i] = bucketNo;
    sendSizesTempLeft[bucketNo]++;
  }

  for (int i = localSize/2; i < localSize; i++){ 
    int bucketNo = lower_bound(pivots, pivots + numPivots, data[i]) - pivots;
    dataToBucket[i] = bucketNo;
    sendSizesTempRight[bucketNo]++;
  }

  // Calculated for the Alltoall sending
  for (int i = 0; i < numBuckets; i++) {
     sendSizes[i] = sendSizesTempRight[i] + sendSizesTempLeft[i];
  } 

  // To tell each process (aka bucket) how much it should expect from other
  // processes (aka chunks)i
  //
 
  double start_time_2c = MPI_Wtime();
  MPI_Alltoall(sendSizes, 1, MPI_INT, receiveSizes, 1, MPI_INT, MPI_COMM_WORLD); 
  double end_time_2c = MPI_Wtime();

  if (proc==0)
	printf("Step 2c: %.4f on %d processors\n", end_time_2c - start_time_2c, numBuckets);

    

  int *sendDispLeft  = (int*) calloc(sizeof(int) * numBuckets, sizeof(int));
  int *sendDispRight = (int*) calloc(sizeof(int) * numBuckets, sizeof(int));

  // Prefix sum 
  for (int i = 1; i < numBuckets; i++) {
     sendDispLeft[i] = sendDispLeft[i-1] + sendSizesTempLeft[i-1];
     sendDispRight[i] = sendDispRight[i-1] + sendSizesTempRight[i-1];
  }

  int *intoBucketOffset = (int *) calloc(sizeof(int) * numBuckets, sizeof(int));

  // This is where we move the elements into the sendBuffer. This is also done
  // in chunks
  for (int i = 0; i < localSize/2; i++){
    int bucketNo = dataToBucket[i];                    //Bucket Number
    int offsetToBucket = sendDispLeft[bucketNo];       //Offset from sendbuf base
    int index = offsetToBucket + intoBucketOffset[bucketNo]++; //Final location

    sendBuf[index] = data[i];
  }

  memset(intoBucketOffset, 0, numBuckets * sizeof(int));

  for (int i = localSize/2; i < localSize; i++){
    int bucketNo = dataToBucket[i];                 
    // Offset from sendbuf base
    int offsetToBucket = sendDispRight[bucketNo];       
    // Final location
    int index = (localSize)/2 + offsetToBucket + intoBucketOffset[bucketNo]++;  
    sendBuf[index] = data[i];
  }

  inplace_merge(sendBuf, sendBuf + localSize/2, sendBuf + localSize);	

  // Prefix sum 
  for (int i = 1; i < numBuckets; i++) {
     sendDisps[i] = sendDisps[i-1] + sendSizes[i-1];
     recvDisps[i] = recvDisps[i-1] + receiveSizes[i-1];
  } 
 
  // Free all of our data
  free(sendDispRight);
  free(sendDispLeft); 
  free(sendSizesTempRight);
  free(sendSizesTempLeft);
  free(intoBucketOffset);
  free(dataToBucket);

}


void parallelSort(float *data, float *&sortedData, int procs, int procId, size_t dataSize, size_t &localSize) {
  // Input:
  //  data[]: input arrays of unsorted data, distributed across p processors
  //  sortedData[]: output arrays of sorted data, initially unallocated

  // STEP 1: Find pivots
  int numPivots = procs - 1;        
  float *pivots = (float *) malloc(sizeof(float) * numPivots);

  double start_time_1 = MPI_Wtime();
  findPivots(data, procs, procId, dataSize, localSize, numPivots, pivots);
  double end_time_1 = MPI_Wtime();

  //STEP 2: Redistribute elements in data to buckets
  int numBuckets = numPivots + 1;

  float *sendBuf = (float*) malloc(sizeof(float) * localSize);
  int *sendSizes = (int*) calloc(sizeof(int) * numBuckets, sizeof(int));
  int *receiveSizes = (int*) calloc(sizeof(int) * numBuckets, sizeof(int));
  // We read sendSizes[i] of elements from sendBuf + sendDispslacement[i]
  int *sendDisps = (int*) calloc(sizeof(int) * numBuckets, sizeof(int));
  // We write receiveSizes[i] of elements to receiveBuf + recvDisplacement[i]
  int *recvDisps = (int*) calloc(sizeof(int) * numBuckets, sizeof(int));

  double start_time_2 = MPI_Wtime();
  redistributeElements(data, localSize, sendBuf, pivots, numPivots,
  sendSizes, receiveSizes, sendDisps, recvDisps,procId);
  double end_time_2 = MPI_Wtime();
 
  // Total number of elements in this bucket
  double start_time_3 = MPI_Wtime();
  int totalMainBucketSize = 0;
  for (int i = 0; i < numBuckets; i++){
  	totalMainBucketSize += receiveSizes[i];
  }
  
  float *receiveBuf = (float *) malloc(sizeof(float) * totalMainBucketSize); 
  
  double end_time_3 = MPI_Wtime();
    
  double start_time_3c = MPI_Wtime();
  MPI_Alltoallv(sendBuf,
                sendSizes, sendDisps,
                MPI_FLOAT, receiveBuf, receiveSizes,
                recvDisps, MPI_FLOAT, MPI_COMM_WORLD);

  double end_time_3c = MPI_Wtime();

  // STEP 3: Sequentially sort the individual buckets
  double start_time_4 = MPI_Wtime();
  sort(receiveBuf, receiveBuf + totalMainBucketSize);
  double end_time_4 = MPI_Wtime();

  // STEP 4: Return sorted chunks
  localSize = totalMainBucketSize;
  sortedData = receiveBuf;

  free(sendBuf);
  free(sendSizes);
  free(receiveSizes);
  free(sendDisps);
  free(recvDisps);
  
  if (procId == 0) {
    printf("Step 1: %.4f on %d processors\n", end_time_1 - start_time_1, procs);
    printf("Step 2: %.4f on %d processors\n", end_time_2 - start_time_2, procs);
    printf("Step 3: %.4f on %d processors\n", end_time_3 - start_time_3, procs);
    printf("Step 3c: %.4f on %d processors\n", end_time_3c - start_time_3c, procs);
    printf("Step 4: %.4f on %d processors\n", end_time_4 - start_time_4, procs);
  }
  // Output:
  //  sortedData[]: output arrays of sorted data
  //  localSize   : number of elements in that bucket
  return;
}
