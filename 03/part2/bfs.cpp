#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstddef>
#include <omp.h>
#include <map>
#include <assert.h>

#include "CycleTimer.h"
#include "bfs.h"
#include "graph.h"

#define ROOT_NODE_ID 0
#define NOT_VISITED_MARKER -1
using namespace std;


struct flag_array {
  int num_set;
  int size;
  char *flag;
};

void vertex_set_clear(vertex_set* list) {
    list->count = 0;
}

void vertex_set_init(vertex_set* list, int count) {
    list->alloc_count = count;
    list->present = (int*)malloc(sizeof(int) * list->alloc_count);
    vertex_set_clear(list);
}


void bottom_up_frontier_clear(flag_array *frontier){

    memset(frontier->flag, 0, frontier->size);
    frontier->num_set = 0;

}


// Take One Step of " Bottom-Up" BFS. For each vertex in the 
// univisited set see if it has a parent in the visited set ( frontier).
// If yes then compute its distance and remove it from the unvisited set.    
void bottom_up_step(graph* g, int *distances, flag_array *frontier, 
                    flag_array *next)
{
     
     // Looping through all the vertices and doing work only for unvisited ones
     #pragma omp parallel for schedule(dynamic,256)
     for (int i = 0; i < g->num_nodes; i++) {

       int node = i;

       if (distances[node] == NOT_VISITED_MARKER) { 

          int start_edge = g->incoming_starts[node];
          int end_edge = (node == g->num_nodes - 1) ? g->num_edges : g->incoming_starts[node + 1];
     
          // Checking to see if any of the parent's neighbors
          // is already in the frontier/ visited set of distances
 	  for (int neighbor = start_edge; neighbor < end_edge; neighbor++) {

              int parent = g->incoming_edges[neighbor];

              if (frontier->flag[parent]){
                 distances[node] = distances[parent] + 1;
                 next->flag[node] = 1;
		 #pragma omp atomic
                 next->num_set++;
     		 break;
              }
          }
        }
     }

    return;
}


void bfs_bottom_up(graph* graph, solution* sol)
{

    // 15-418/618 students:
    //
    // You will need to implement the "bottom up" BFS here as
    // described in the handout.
    //
    // As a result of your code's execution, sol.distances should be
    // correctly populated for all nodes in the graph.
    //
    // As was done in the top-down case, you may wish to organize your
    // code by creating subroutine bottom_up_step() that is called in
    // each step of the BFS process.


    int num_nodes = graph->num_nodes;

    char *frontier_flag = (char *) calloc(graph->num_nodes, sizeof(char));
    char *next_flag = (char *) calloc(graph->num_nodes, sizeof(char));

    flag_array *frontier = (flag_array *) malloc(sizeof(flag_array));
    frontier->flag = frontier_flag;
    frontier->size = num_nodes;

    flag_array *next = (flag_array *) malloc(sizeof(flag_array));
    next->flag = next_flag;
    next->size = num_nodes;

    // Initialize all distances to NOT_VISITED_MARKER and add to unvisited 
    for (int i=0; i < graph->num_nodes; i++) {
        sol->distances[i] = NOT_VISITED_MARKER;
    }

    // setup frontier with the root node
    frontier->flag[ROOT_NODE_ID] = 1;
    frontier->num_set = 1;

    // Root distance
    sol->distances[ROOT_NODE_ID] = 0;

    // Loop through graph until all vertices visited
    while (frontier->num_set != 0){

        bottom_up_frontier_clear(next);

        bottom_up_step(graph, sol->distances, frontier, next);

        // swap pointers
        flag_array *tmp = frontier;
        frontier = next;
        next = tmp;
    }
}



// Take one step of "top-down" BFS.  For each vertex on the frontier,
// follow all outgoing edges, and add all neighboring vertices to the
// new_frontier.
void top_down_step_seq(graph* g, vertex_set* frontier, vertex_set* new_frontier,
                       int* distances)
{
    // Looping through nodes in frontier
    for (int i=0; i<frontier->count; i++) {

        int node = frontier->present[i];

        int start_edge = g->outgoing_starts[node];
        int end_edge = (node == g->num_nodes-1) ? g->num_edges : g->outgoing_starts[node+1];

        // attempt to add all neighbors to the new frontier
        for (int neighbor=start_edge; neighbor<end_edge; neighbor++) {
            int outgoing = g->outgoing_edges[neighbor];

            // This whole block needs to be atomic
            {
              if (distances[outgoing] == NOT_VISITED_MARKER) {
                  distances[outgoing] = distances[node] + 1; 
                  int index = new_frontier->count++;
                  new_frontier->present[index] = outgoing;
              }
            }
        }
    }
}

// Implements top-down BFS.
//
// Result of execution is that, for each node in the graph, the
// distance to the root is stored in sol.distances.
void bfs_top_down_seq(graph* graph, solution* sol) {

    vertex_set list1;
    vertex_set list2;
    vertex_set_init(&list1, graph->num_nodes);
    vertex_set_init(&list2, graph->num_nodes);

    vertex_set* frontier = &list1;
    vertex_set* new_frontier = &list2;

    // initialize all nodes to NOT_VISITED
    for (int i=0; i<graph->num_nodes; i++)
        sol->distances[i] = NOT_VISITED_MARKER;

    // setup frontier with the root node
    frontier->present[frontier->count] = ROOT_NODE_ID;
    frontier->count++;
    sol->distances[ROOT_NODE_ID] = 0;

    while (frontier->count != 0) {

        vertex_set_clear(new_frontier);

        top_down_step_seq(graph, frontier, new_frontier, sol->distances);

        // swap pointers
        vertex_set* tmp = frontier;
        frontier = new_frontier;
        new_frontier = tmp;
    }
}

// Take one step of "top-down" BFS.  For each vertex on the frontier,
// follow all outgoing edges, and add all neighboring vertices to the
// new_frontier.
void top_down_step(
    graph* g,
    vertex_set* frontier,
    vertex_set* new_frontier,
    int* distances)
{
    // Looping through nodes in frontier
    #pragma omp parallel for 
    for (int i = 0; i < frontier->count; i++) {

        int node = frontier->present[i];

        int start_edge = g->outgoing_starts[node];
        int end_edge = (node == g->num_nodes-1) ? g->num_edges : g->outgoing_starts[node+1];

        // attempt to add all neighbors to the new frontier
        for (int neighbor = start_edge; neighbor < end_edge; neighbor++) {
            int outgoing = g->outgoing_edges[neighbor];

            int visited = distances[outgoing];

            if (visited == NOT_VISITED_MARKER && 
              __sync_bool_compare_and_swap(distances + outgoing, 
              NOT_VISITED_MARKER, (distances[node] + 1))) {
                
                int index = __sync_fetch_and_add(&new_frontier->count, 1);
                new_frontier->present[index] = outgoing;
            }
        }
    }
}

// Implements top-down BFS.
//
// Result of execution is that, for each node in the graph, the
// distance to the root is stored in sol.distances.
void bfs_top_down(graph* graph, solution* sol) {

    vertex_set list1;
    vertex_set list2;
    vertex_set_init(&list1, graph->num_nodes);
    vertex_set_init(&list2, graph->num_nodes);

    vertex_set* frontier = &list1;
    vertex_set* new_frontier = &list2;

    // initialize all nodes to NOT_VISITED
    for (int i=0; i<graph->num_nodes; i++)
        sol->distances[i] = NOT_VISITED_MARKER;

    // setup frontier with the root node
    frontier->present[frontier->count] = ROOT_NODE_ID;
    frontier->count++;
    sol->distances[ROOT_NODE_ID] = 0;

    while (frontier->count != 0) {

        vertex_set_clear(new_frontier);

        top_down_step(graph, frontier, new_frontier, sol->distances);

        // swap pointers
        vertex_set* tmp = frontier;
        frontier = new_frontier;
        new_frontier = tmp;
    }
}

void bfs_hybrid(graph *graph, solution *sol){
 

    // When frontier hits this size, we switch from top_down to bottom_up
    int limit = 5000000;
    // Top Down set up
    vertex_set list1;
    vertex_set list2;
    vertex_set_init(&list1, graph->num_nodes);
    vertex_set_init(&list2, graph->num_nodes);

    vertex_set* top_down_frontier = &list1;
    vertex_set* top_down_next = &list2;

    // initialize all nodes to NOT_VISITED
    for (int i=0; i<graph->num_nodes; i++)
        sol->distances[i] = NOT_VISITED_MARKER;

    // setup top_down_frontier with the root node
    top_down_frontier->present[top_down_frontier->count] = ROOT_NODE_ID;
    top_down_frontier->count++;
    sol->distances[ROOT_NODE_ID] = 0;

    while (top_down_frontier->count > 0 && top_down_frontier->count < limit) {
      
        vertex_set_clear(top_down_next);

        top_down_step(graph, top_down_frontier, top_down_next, sol->distances);

        // swap pointers
        vertex_set* tmp = top_down_frontier;
        top_down_frontier = top_down_next;
        top_down_next = tmp;
    }

    if (top_down_frontier->count == 0) return;

    // We need to try bottom up now


    // Set up botom up
    int num_nodes = graph->num_nodes;

    char *bottom_up_frontier_flag = (char *) calloc(graph->num_nodes, sizeof(char));
    char *bottom_up_next_flag = (char *) calloc(graph->num_nodes, sizeof(char));

    flag_array *bottom_up_frontier = (flag_array *) malloc(sizeof(flag_array));
    bottom_up_frontier->flag = bottom_up_frontier_flag;
    bottom_up_frontier->size = num_nodes;
    bottom_up_frontier->num_set = top_down_frontier->count;

    flag_array *bottom_up_next = (flag_array *) malloc(sizeof(flag_array));
    bottom_up_next->flag = bottom_up_next_flag;
    bottom_up_next->size = num_nodes;
  
    // Transfers over the data from previous frontier data structure
    // to current frontier data structure
    for (int i = 0; i < top_down_frontier->count; i++) {
      int node = top_down_frontier->present[i];
      bottom_up_frontier->flag[node] = 1;
    }

    while (bottom_up_frontier->num_set != 0){

        bottom_up_frontier_clear(bottom_up_next);

        bottom_up_step(graph, sol->distances, bottom_up_frontier, bottom_up_next);

        // swap pointers
        flag_array *tmp = bottom_up_frontier;
        bottom_up_frontier = bottom_up_next;
        bottom_up_next = tmp;
    }

    return;

}
