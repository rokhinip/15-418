#include <map>
using namespace std;

typedef map<char, string> Symbol_map;
typedef map<string, char> Rev_symbol_map;

typedef struct Encoding{
  
  Symbol_map symbol_map;		
  string encoded_message;

} Encoding;

Rev_symbol_map *build_reverse_symbol_map(Encoding *encoded_input);
string decode_msg_chunk(string *encoded_message, Rev_symbol_map *rev_symbol_map,
                        int start_of_decode, int end_of_decode);
