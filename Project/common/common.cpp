#include <string>
#include <stdio.h>
#include <string.h>

#include "common.h"

using namespace std;

typedef map<string, char> Rev_symbol_map;
typedef map<char, string> Symbol_map;

Rev_symbol_map *build_reverse_symbol_map(Encoding *encoded_input){
 
  Rev_symbol_map *rev_symbol_map = new Rev_symbol_map(); 
  Symbol_map symbol_map = encoded_input->symbol_map;

  for (Symbol_map::iterator it = symbol_map.begin(); 
       it != symbol_map.end(); ++it) {
    (*rev_symbol_map)[it->second] = it->first;
  }
  return rev_symbol_map;
}

string decode_msg_chunk(string *encoded_msg, Rev_symbol_map *rev_symbol_map,
                         int start_of_decode, int end_of_decode){
 
  int len = end_of_decode - start_of_decode + 1;
  string encoded_message = (*encoded_msg).substr(start_of_decode, len);
  
  string decoded_msg = "";
  int code_word_start = 0;
  int code_word_length = 1;
  while (code_word_start < (int) encoded_message.length()) {

    string prefix = encoded_message.substr(code_word_start, code_word_length);
    if ((*rev_symbol_map).count(prefix)) { 
      decoded_msg += (*rev_symbol_map)[prefix];
      code_word_start += code_word_length;
      code_word_length = 1;
    } else {
      code_word_length++;
    }
  }

  return decoded_msg;

}
