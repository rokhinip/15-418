#include <string>
#include <stdio.h>
#include <string.h>

#include "../huffman.h"

#define NUM_CHARS 256

using namespace std;

string huffman_decode(Encoding *encoding) {

  Rev_symbol_map *rev_symbol_map = build_reverse_symbol_map(encoding);
  string *encoded_msg = &encoding->encoded_message;
  string decoded_msg = decode_msg_chunk(encoded_msg, rev_symbol_map,
                                        0, (*encoded_msg).length() - 1);
  delete rev_symbol_map; 
  delete encoding;

  return decoded_msg;
  
}
