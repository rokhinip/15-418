#include <stdio.h>
#include <string.h>
#include <string>
#include <iostream>
#include <fstream>

#include "common/cycle_timer.h"
#include "huffman.h"

using namespace std;

float compare_strings(string s1, string s2){

  float hits = 0.0; 
  for (unsigned int i = 0; i < min(s1.length(), s2.length()); i++) {
     char s1_char = s1.at(i);
     char s2_char = s2.at(i);
     if (s1_char == s2_char) {
       hits++;
     }
  }

  return (hits/(max(s1.length(), s2.length())))*100;
}

int main(int argc, char **argv){

  /* Parse command line args specifying test file to run */
  if (argc < 3) {
    cout << *argv << endl;
    printf("Please specify a test case to run by using the --test flag!\n");
    return 1;
  }
 
  if (strcmp(argv[1], "--test") && strcmp(argv[1], "-t")){
    printf("Please specify a test case to run by using the --test flag!\n");
    return 1;
  }

  /* We now read the file line by line and build up a string */
  char *test_file_name = argv[2];
  ifstream test_file_readin(test_file_name);

  string line_read;
  string full_test_contents;

  while (getline(test_file_readin, line_read)){
    full_test_contents += line_read;
    full_test_contents.push_back('\n');
  }
 
  /* Convert strings to char * arrays */ 
  int length = full_test_contents.length();
  char *message = new char[length];
  strcpy(message, full_test_contents.c_str());

  /* Serial encode the message */
  Encoding *encoding = huffman_encode(message, length);

  /* Parallel decode the message */
  double start_time = CycleTimer::currentSeconds(); 
  string decoded_msg = huffman_decode(encoding);
  double end_time = CycleTimer::currentSeconds();

  float match_percentage = compare_strings(full_test_contents, decoded_msg);
  cout << "Test case: " << test_file_name << endl;
  cout << "Time taken for decoding: " <<  end_time - start_time << endl; 
  cout << "Match: " << match_percentage << endl;
  return 0;
}
