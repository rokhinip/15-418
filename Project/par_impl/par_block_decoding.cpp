#include <string>
#include <map>
#include <iostream>
#include <stdio.h>
#include <assert.h>
#include <vector>
#include <mutex>
#include <thread>

#include "../huffman.h"
#define NUM_THREADS 6 

using namespace std;
// Vector Mutex Struct  
struct Word_size_per_block {
    mutex mtx;         
    vector<int> V;
};

// We define a struct to contain all our params so that it's easier to pass it
// around
typedef struct Params {

  string *encoded_msg; 
  Rev_symbol_map *rev_symbol_map; 
  Word_size_per_block *word_sizes;

  int blockID;
  int start_of_block;
  int end_of_block;

} Params;

/********************************** Debug info *******************************/
void print_info(Word_size_per_block *word_sizes){
  
  for (int i = 0; i < NUM_THREADS; i++) {
    printf("Block [\n");
    vector<int> V = word_sizes[i].V;
    for (vector<int>::iterator it = V.begin(); it != V.end(); ++it){
      printf("%d, ", *it);
    }

    printf("\n]\n");
  }
}

/************************ Finding EOC within block ************************/

// We try to find the EOCs within a block. 
// If we are able to finish decoding exactly within block, return 0 since no 
// overflow, else return 0
int find_EOC_single_block(Params *params){

  string encoded_string = *(params->encoded_msg);
  int code_word_start = params->start_of_block;
  int blockID = params->blockID;
  int end_of_block = params->end_of_block;
  Rev_symbol_map *rev_symbol_map = params->rev_symbol_map;
  Word_size_per_block *word_sizes = params->word_sizes;

  int code_word_length = 1;

  while (code_word_start + code_word_length <= end_of_block) {
     
     string prefix = encoded_string.substr(code_word_start, code_word_length);
     
     if ((*rev_symbol_map).count(prefix)) { 
           
        // Record the ending index of the code word  
        word_sizes[blockID].mtx.lock();
        int code_word_end_index = code_word_start + code_word_length - 1;
        word_sizes[blockID].V.push_back(code_word_end_index);
        word_sizes[blockID].mtx.unlock();

        // Reset start index and length for decoding the next code word
        code_word_start += code_word_length;
        code_word_length = 1;
      } else {
        code_word_length++;
      }			
  }

    // Read the ending index of the Last Element Encoded
    word_sizes[blockID].mtx.lock();
    int last_code_word_end = word_sizes[blockID].V.back();
    word_sizes[blockID].mtx.unlock();
     
    //if EOB is an EOC, then we're done. Indicate if there is a overflow
    return (last_code_word_end != end_of_block - 1);
}

/************************ Finding EOC from overflow **************************/

void update_params(Params *params, int &index_into_V){
   
  int encoded_string_length = (*(params->encoded_msg)).length();
  int chunk_length = (encoded_string_length + NUM_THREADS-1)/NUM_THREADS;

  int blockID = ++params->blockID;
  int start_of_block = blockID*chunk_length;
  int end_of_block = 
    ((start_of_block + chunk_length) <  encoded_string_length) ?
      start_of_block + chunk_length : encoded_string_length; 

  params->start_of_block = start_of_block;
  params->end_of_block = end_of_block;
  index_into_V++;

}

void find_EOC_till_sync_point(Params *params, int index_into_V) {

  // We pull out params from the struct 
  string encoded_string = *(params->encoded_msg);
  int encoded_string_length = encoded_string.length();
  Rev_symbol_map rev_symbol_map = *params->rev_symbol_map;
  int blockID = params->blockID;
  Word_size_per_block *word_sizes = params->word_sizes; 
  int start_of_block = params->start_of_block;
  int end_of_block = params->end_of_block;


  int code_word_start = start_of_block;
  // We start from where the previous block's words ended if it didn't match up
  // to EOB
  if (blockID != 0){
    int end_of_prev_code_word = word_sizes[blockID-1].V.back();
    // Start from where the previous block ended
    if (end_of_prev_code_word != start_of_block - 1) {
      code_word_start = end_of_prev_code_word + 1;
    }
  }
  
  int code_word_length = 1;
  while (code_word_start + code_word_length < encoded_string_length){

     string prefix = encoded_string.substr(code_word_start, code_word_length);
     if (rev_symbol_map.count(prefix) == 0) { //Haven't found a word 
       code_word_length++;
       continue;
     }

     int code_word_end_index = code_word_start + code_word_length - 1;
     if (code_word_end_index == end_of_block - 1) { // Reached EOB
       
       return;

     } else if (code_word_end_index >= end_of_block) { // Overflowed again
      
       index_into_V = -1;
       update_params(params, index_into_V);
     
     } else { // Found EOC in the middle of current block
       
       // Erase what has been decoded till here by the initial thread of that block
       word_sizes[blockID].mtx.lock();
       vector<int> V = word_sizes[blockID].V;

       while (index_into_V < (int) V.size() &&
              V.at(index_into_V) < code_word_end_index){
         V.erase(V.begin() + index_into_V);
       }

       word_sizes[blockID].V = V; 
       word_sizes[blockID].mtx.unlock();

       if (index_into_V < (int) V.size() &&
           V.at(index_into_V) == code_word_end_index) { // Sync point found
         return;
       } else {

         // Not a synchronization point, take note of index and continue
         word_sizes[blockID].mtx.lock();
         word_sizes[blockID].V.insert(
            word_sizes[blockID].V.begin() + index_into_V, 
            code_word_end_index);
         index_into_V++;
         word_sizes[blockID].mtx.unlock();
       }

       code_word_start = code_word_end_index + 1;
       code_word_length = 1;
     }
  }     
}

void find_EOC_from_overflow(Params *params){

  int index_into_V = -1;
  update_params(params, index_into_V);
  if (params->blockID >= NUM_THREADS){
    // We overflow-ed past final block
    return;
  }

  find_EOC_till_sync_point(params, index_into_V);
  return;
}

void find_EOCs(Params *params){
 
  // Find EOC within blocks in parallel 
  int overflow[NUM_THREADS]; 
  #pragma omp parallel for
  for (int i = 0; i < NUM_THREADS; i++){
    overflow[i] = find_EOC_single_block(&params[i]);
  }
  // Find EOC from overflow
  #pragma omp parallel for
  for (int i = 0; i < NUM_THREADS; i++){
    if (overflow[i]) find_EOC_from_overflow(&params[i]);
  }

  return;
}

/************************ Decoding after finding EOCs ************************/

string decode_msg_chunk(Params *params,int start_of_decode){
  
  string encoded_string = *(params->encoded_msg);
  Rev_symbol_map rev_symbol_map = *(params->rev_symbol_map); 
  int blockID = params->blockID;
  vector<int> V = params->word_sizes[blockID].V;

   
  // We use the precomputed information in vector V to calculate this  
  string decoded_msg = "";
  int start_index = start_of_decode;
  vector<int>::iterator end_index_ptr;
 
  for (end_index_ptr = V.begin(); end_index_ptr != V.end(); end_index_ptr++){
    int end_index = *end_index_ptr;
    int len = end_index - start_index + 1;
    string code_word = (encoded_string.substr(start_index, len));
    decoded_msg += (rev_symbol_map[code_word]);
    start_index = end_index + 1;
    
  }
   
  return decoded_msg;
}


void decode_block(Params *params, string *output_buf){

  int blockID = params->blockID;
  int start_of_block = params->start_of_block;
  Word_size_per_block *word_sizes = params->word_sizes;
  vector<int> V = word_sizes[blockID].V;
  
  int start_of_decode = start_of_block; 

  if (blockID != 0){
    int end_of_prev_code_word = word_sizes[blockID-1].V.back();
    // Start from where the previous block ended
    if (end_of_prev_code_word != start_of_block - 1) {
       start_of_decode = end_of_prev_code_word + 1;
    }
  }

 output_buf[blockID] = decode_msg_chunk(params, start_of_decode);
 return;

}

string decode_blocks(Params *params){
  
  string *output_buf = new string[NUM_THREADS];
  // Decode the message in parallel
  #pragma omp parallel for
  for (int i = 0; i < NUM_THREADS; i++){
    decode_block(&params[i], output_buf);
  }
  
  string decoded_msg = "";
  for (int i = 0; i < NUM_THREADS; i++){
    decoded_msg += output_buf[i];
  }
   
  return decoded_msg;
}

/************************ Main functions **********************************/

void initialize_params_for_decode(Params *params, string *encoded_msg,
                                  Rev_symbol_map *rev_symbol_map, 
                                  Word_size_per_block *word_sizes){

  int encoded_string_length = (*encoded_msg).length(); 
  int chunk_length = (encoded_string_length + NUM_THREADS-1)/NUM_THREADS;

  for (int blockID = 0; blockID < NUM_THREADS; blockID++){

    int start_of_block = blockID * chunk_length;
    int end_of_block = 
          ((start_of_block + chunk_length) <  encoded_string_length) ?
            start_of_block + chunk_length : encoded_string_length;

    params[blockID].encoded_msg = encoded_msg;
    params[blockID].rev_symbol_map = rev_symbol_map;
    params[blockID].word_sizes = word_sizes;
    params[blockID].blockID = blockID; 
    params[blockID].start_of_block = start_of_block;
    params[blockID].end_of_block = end_of_block;

  }
}

string huffman_decode(Encoding *encoded_input){

  string *encoded_msg = &encoded_input->encoded_message;
  Rev_symbol_map *rev_symbol_map = build_reverse_symbol_map(encoded_input);
  Word_size_per_block *word_sizes = new Word_size_per_block[NUM_THREADS]; 
  Params *params = new Params[NUM_THREADS];
 
  // We initialize the params for finding the EOCS 
  initialize_params_for_decode(params, encoded_msg, rev_symbol_map, word_sizes);

  find_EOCs(params);
  // We need to reinitialize the blockIDs for each thread for the decoding step
  initialize_params_for_decode(params, encoded_msg, rev_symbol_map, word_sizes);
  
  string decoded_msg = decode_blocks(params);
  return decoded_msg;
}
