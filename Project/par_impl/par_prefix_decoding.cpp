#include <string>
#include <stdio.h>
#include <iostream>
#include <string.h>

#include "../huffman.h"

#define NUM_CHARS 256

using namespace std;

string decode_msg_chunk(string encoded_message, Rev_symbol_map rev_symbol_map){

  int full_msg_length = encoded_message.length();

  int max_prefix_size = 0;
  Rev_symbol_map::iterator it;
  for (it = rev_symbol_map.begin(); it != rev_symbol_map.end(); ++it) {
    int encoding_length = it->first.length();
    if (encoding_length > max_prefix_size){
      max_prefix_size = encoding_length;
    }
  }
  
  string decoded_msg = "";
  int code_word_start = 0;
  while (code_word_start < full_msg_length) {
    int prefix_size_match = 0;
    #pragma omp parallel for num_threads(4)
    for (int prefix_size = 1; prefix_size <= max_prefix_size; prefix_size++) {
      
      if (code_word_start + prefix_size > full_msg_length) continue;
      
      string prefix = encoded_message.substr(code_word_start, prefix_size);
      if (rev_symbol_map.count(prefix) > 0){
        // We are guaranteed that only 1 prefix will match   
        prefix_size_match = prefix_size;
        decoded_msg += rev_symbol_map[prefix];
      } 
    }
    code_word_start += prefix_size_match;
  }
  return decoded_msg;
}


string huffman_decode(Encoding *encoding) {

  Rev_symbol_map *rev_symbol_map = build_reverse_symbol_map(encoding);
  string encoded_msg = encoding->encoded_message;
  string decoded_msg = decode_msg_chunk(encoded_msg, *rev_symbol_map);
  delete rev_symbol_map; 
  delete encoding;

  return decoded_msg;
  
}
