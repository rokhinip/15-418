#include <map>
#include <string>
#include "common/common.h"

using namespace std;

Encoding *huffman_encode(char *message, int length);
string huffman_decode(Encoding *encoded_input); 

