import os
import subprocess

DIR_NAME = "tests"
SEQ_LOG = "seq_log.txt"
PAR_BLK_LOG = "par_block_log.txt"
PAR_CHAR_LOG = "par_char_log.txt"
PADDING_LOG = "padding_log.txt"

try:
  subprocess.call(["make","clean"])
  subprocess.call(["make"])
except:
  print "Couldn't run make. Please check the Makefile"

for test_file_name in os.listdir(DIR_NAME):
  subprocess.call("./seq -t " + os.path.join(DIR_NAME, test_file_name) + " >> " + SEQ_LOG, shell=True)
  subprocess.call("./par_char -t " + os.path.join(DIR_NAME, test_file_name) + " >> " + PAR_CHAR_LOG, shell=True)
  subprocess.call("./par_block -t " + os.path.join(DIR_NAME, test_file_name) + " >> " + PAR_BLK_LOG, shell=True)
  subprocess.call("./with_padding -t " + os.path.join(DIR_NAME, test_file_name) + " >> " + PADDING_LOG, shell=True)


with open(SEQ_LOG, "r") as seq_file:
  with open(PAR_BLK_LOG, "r") as par_blk_file:
    with open(PAR_CHAR_LOG, "r") as par_chr_file:
      with open(PADDING_LOG, "r") as pad_file:
        seq_file_contents = seq_file.readlines()
        par_blk_file_contents = par_blk_file.readlines()
        par_chr_file_contents = par_chr_file.readlines()
        padding_file_contents = pad_file.readlines()
        
        for i in xrange(len(seq_file_contents)):
          line = seq_file_contents[i]

          if "Test case" in line and line in par_blk_file_contents and \
            line in padding_file_contents and line in par_chr_file_contents:
            
            seq_timings = seq_file_contents[i+1].split(":")[1]
            seq_accuracy = eval(seq_file_contents[i+2].split(":")[1])

            par_blk_timings = par_blk_file_contents[i+1].split(":")[1]
            par_blk_accuracy = eval(par_blk_file_contents[i+2].split(":")[1])
            
            par_chr_timings = par_chr_file_contents[i+1].split(":")[1]
            par_chr_accuracy = eval(par_chr_file_contents[i+2].split(":")[1])

            pad_timings = padding_file_contents[i+1].split(":")[1]
            pad_accuracy = eval(padding_file_contents[i+2].split(":")[1])

            test_case_name = line.split(":")[1]

            with open("final_results.txt", "w") as f:
              to_write = "Test: " + test_case_name
              
              if seq_accuracy != 100 or par_blk_accuracy != 100 \
                or pad_accuracy != 100 or par_chr_accuracy != 100:
                to_write += (
                "*************Decoding results are incorrect!*************\n" + \
                "Seq accuracy: " + str(seq_accuracy) + "\n" + \
                "Par Block accuracy: " + str(par_blk_accuracy) + "\n" + \
                "Par Char accuracy: " + str(par_chr_accuracy) + "\n" + \
                "Pad accuracy: " + str(pad_accuracy) + "\n")

                f.write(to_write)
                continue

              to_write += ("Sequential timing: " + seq_timings + \
              "Parallel block timing: " + par_blk_timings + \
              "Speedup of Block parallel: " + \
              str(eval(seq_timings)/eval(par_blk_timings)) + "x\n" + \
              "Parallel char timing: " + par_chr_timings + \
              "Speedup of Char parallel: " + \
              str(eval(seq_timings)/eval(par_chr_timings)) + "x\n" + \
              "Padding timing: " + pad_timings + \
              "-------------------------------------------------------------\n")
              f.write(to_write)

try:
  subprocess.call("make clean", shell=True)
except:
  pass
