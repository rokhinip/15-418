from PIL import Image
import copy
import math
import datetime
import subprocess

'''
    Convert RGB to YUV
'''
def rgb_to_yuv(pixel):
    R = pixel[0]
    G = pixel[1]
    B = pixel[2]
    Y  = R *  0.29900 + G *  0.58700 + B *  0.11400
    Cb = R * -0.16874 + G * -0.33126 + B *  0.50000 + 128
    Cr = R *  0.50000 + G * -0.41869 + B * -0.08131 + 128
    return (Y,Cr,Cb)


'''
    Convert YUV to RGB
'''
def yuv_to_rgb(y, cb, cr):
    r = y + 1.402 * (cr-128)
    g = y - .34414 * (cb-128) -  .71414 * (cr-128)
    b = y + 1.772 * (cb-128)
    return (r, g, b)

'''

    shift the 8 * 8 Block by offset to fit range. 

'''
def shift_image(i,j,image,offset):

    # Upper Left Corner Co-ordinates.
    left_upper_x = i;
    left_upper_y = j;

    for u in xrange(0,8):
        for v in xrange(0,8):
            shifted_pixel = []
            for dim in range(0,3):
                shifted_pixel += [image[left_upper_y + v][ left_upper_x + u][dim]+offset]
            image[left_upper_y+v][left_upper_x + u] = (int(shifted_pixel[0]),int(shifted_pixel[1]),int(shifted_pixel[2]))




''' 
    Compute DCT cosine Value for pixel[u][v] relative to box with left 
    upper corner at (left_upper_x,left_upper_y) 
'''
def cosine_uv(u,v,image,left_upper_x,left_upper_y):
    # image = [[ 0 for x in range(0,8) ] for y in range(0,8)]
    alpha_u = (1.0/math.sqrt(2)) if (u==0) else 1
    alpha_v = (1.0/math.sqrt(2)) if (v==0) else 1
    final_value = [0.0,0.0,0.0]

    # Sum up to get DCT values
    
    for dimension in range(0,3):
        for x in xrange(0,8):
            for y in xrange(0,8):
                 final_value[dimension] = final_value[dimension] + image[left_upper_y + y][left_upper_x + x][dimension] * math.cos(((2*x + 1)*u*math.pi)/16) * math.cos(((2*y + 1)*v*math.pi)/16)  
        final_value[dimension] = 1.0*alpha_u * alpha_v * final_value[dimension] / 4.0 
    return (final_value[0],final_value[1],final_value[2])





''' 

    Compute inverse DCT cosine Value for pixel[u][v] relative to box with left 
    upper corner at (left_upper_x,left_upper_y) 

'''
def cosine_uv_inverse(x,y,image,left_upper_x,left_upper_y):
    # image = [[ 0 for x in range(0,8) ] for y in range(0,8)]
   final_value = [0.0,0.0,0.0]
   

   # Sum up to get DCT values
   for dimension in range(0,3):
        for u in xrange(0,8):
            for v in xrange(0,8):
                alpha_u = (1.0/math.sqrt(2)) if (u==0) else 1
                alpha_v = (1.0/math.sqrt(2)) if (v==0) else 1 
                final_value[dimension] = final_value[dimension] + 1.0 * alpha_u * alpha_v * image[left_upper_y + v][left_upper_x + u][dimension] * math.cos(((2*x + 1)*u*math.pi)/16) * math.cos(((2*y + 1)*v*math.pi)/16)  
        final_value[dimension] =  final_value[dimension] / 4.0 
   return (int(final_value[0]+128),int(final_value[1]+128),int(final_value[2]+128))




'''
    Convert YUV in 8 * 8 box into DCT representation. 
    flag = 1 normal
    flag = 0 inverse
'''
def convertBoxDCT (i,j,image,flag):
    # Upper Left Corner Co-ordinates.
    left_upper_x = i;
    left_upper_y = j;
    
    # copy needed to ensure correctness  
    image_copy =[ [(0,0,0) for i in range(8)]  for i in range(8) ]
    
    # iterate over the box and calculate DCT values and update it.
    for u in xrange(0,8):
         for v in xrange(0,8):
             if flag:
                image_copy[v][u] = cosine_uv(u,v,image,left_upper_x,left_upper_y)
             else:
                image_copy[v][u] = cosine_uv_inverse(u,v,image,left_upper_x,left_upper_y) 
    for u in xrange(0,8):
         for v in xrange(0,8):
             image[left_upper_y + v][left_upper_x + u] = image_copy[v][u]  


''' 
    Quantize or dequantize the 8 * 8 Block depending on the flag
    flag = 1 for quantize
    flag = 0 for dequantize
'''
def quantize_dequantize(i,j,image,flag):
    
    # Common Quantize Matrix used for JPEG
    quantize = [[8, 16, 19, 22, 26, 27, 29, 34], [16, 16, 22, 24, 27, 29, 34, 37], [19, 22, 26, 27, 29, 34, 34, 38], [22, 22, 26, 27, 29, 34, 37, 40], [22, 26, 27, 29, 32, 35, 40, 48], [26, 27, 29, 32, 35, 40, 48, 58], [26, 27, 29, 34, 38, 46, 56, 69], [27, 29, 35, 38, 46, 56, 69, 83]]

#         [[16,11,10,16,24,40,51,61],
#         [12,12,14,19,26,58,60,55],
#         [14,13,16,24,40,57,69,56],
#         [14,17,22,29,51,87,80,62],
#         [18,22,37,56,68,109,103,77],
#         [24,35,55,64,81,104,113,92],
#         [49,64,78,87,103,121,120,101],
#         [72,92,95,98,112,100,103,99]]
    # Upper Left Corner Co-ordinates.
    left_upper_x = i;
    left_upper_y = j;
    
    for u in xrange(0,8):
        for v in xrange(0,8):
            quantized_pixel = []
            for dim in range(0,3):
                if (flag):
                    quantized_pixel += [image[left_upper_y + v][ left_upper_x + u][dim] / quantize[v][u]]
                else:
                    quantized_pixel += [image[left_upper_y + v][ left_upper_x + u][dim] * quantize[v][u]]
            image[left_upper_y+v][left_upper_x + u] = (int(round(quantized_pixel[0])),int(round(quantized_pixel[1])),int(round(quantized_pixel[2])))


''' 
    Main Wrapper for Encoding an Image to JPEG
'''
def  decompress_to_jpeg(image):
     height = len(image)
     width = len(image[0])
     # Convert RGB to YrCbCr
     #for u in xrange(0,width):
     #   for v in xrange(0,height):
     #       image[v][u] = rgb_to_yuv(image[v][u])
     print " Done YUV " 
     for i in range(0,width/8):
        for j in range(0,height/8):
            #assert(i < width and j <  height)
            shift_image(i*8,j*8,image,-128)
            convertBoxDCT(i*8,j*8,image,1)
            quantize_dequantize(i*8,j*8,image,1) 
     print("Final Image To Encode !") #,image)
     final_string = "" 
     final_string_grid =  ["" for j in range(width)]   
     text_file = open ("image_output.txt","w")
     for i in range(0,height):
          for j in range(0,width): 
            for dim in range(0,3):
               final_string_grid[j]+= str(image[i][j][dim]) + " "
            final_string_grid[j]+=":"
          text_file.write("".join(final_string_grid) + "\n")
          #final_string = final_string + "".join(final_string_grid) + "\n"
          final_string_grid =  ["" for j in range(width)]   
          #final_string  = final_string + "\n"
     text_file.close() 
     # Final String to Encode.
     print " Final String Done \n" #+ final_string

def parse_string_to_quantized_image(string_from_file):
    
     split_newline = [] 
     with open(string_from_file) as fp:
        for line in fp:
            split_newline+=[line]

     # Try to Get the Values Back From the String
     split_triples = [i.split(":") for i in split_newline]
     
     height = len(split_triples)
     width = len(split_triples[0])-1
     pixel_values = [ [(0,0,0) for i in range(width)]  for i in range(height)] 
     print " width " + str(width)
     print " height " + str(height)
     for i in range(height):
        for j in range(width):
            split_space = split_triples[i][j].split(" ") 
            try:
                pixel_values[i][j] = (int(split_space[0]),int(split_space[1])
                            ,int(split_space[2]))
            except:
                print " Exception BRO ", i, " ", j
                pixel_values[i][j] = (255,255,255)
     print " Parsed Pixel " # + str(pixel_values) + "\n"
     for i in range(0,width/8):
        for j in range(0,height/8):
            quantize_dequantize( i*8,j*8,pixel_values,0)
            #print " DeQuantized " #+ str(pixel_values) + "\n"
            
            # Inverse DCT Transformation 
            convertBoxDCT(i*8,j*8,pixel_values,0)
            #print " Inverse DCT " #+ str(pixel_values) + "  \n\n\n"
            
     print " Final Image size : " + str(len(pixel_values)) + " : " + str(len(pixel_values[0]))   #+ str(pixel_values) + " \n"
     image_width = (width/8)*8
     image_height = (height/8)*8
     img = Image.new('RGB',(image_width, image_height),"white")
     print "img size ", img.size[0], " ", img.size[1]  
     img_pixels = img.load()
     for i in range(image_width):
        for j in range(image_height):
            #print "i ",i , " j ", j, pixel_values[j][i] 
            img_pixels[(i,j)] = pixel_values[j][i]
     file_name = "output"+ str(datetime.datetime.now().microsecond) + ".tiff"
     img.save(file_name,"TIFF")
     print " Wrote Image " , file_name 
     #text_file = open("image_pixel_final.txt","w")
     #text_file.write(str(pixel_values)) 
     #text_file.close() 
    
im = Image.open("sample3.gif").convert("RGB")
pix = im.load()
width = im.size[0] 
height = im.size[1] 


image = [ [0 for i in range(im.size[0])]  for i in range(im.size[1]) ]

for i in range(width):
    for j in range(height):
        image[j][i] = pix[i,j]

decompress_to_jpeg(image)
#parse_string_to_quantized_image("test_output.txt")
#./par -t image_output.txt

subprocess.call("./par -t ./image_output.txt",shell=True)
parse_string_to_quantized_image("test_output.txt")

arr = [
        [52, 55, 61, 66, 70, 61, 64, 73], 
        [63, 59, 55, 90, 109, 85, 69, 72], 
        [62, 59, 68, 113, 144, 104, 66, 73], 
        [63, 58, 71, 122, 154, 106, 70, 69], 
        [67, 61, 68, 104, 126, 88, 68, 70],
        [79, 65, 60, 70, 77, 68, 58, 75],
        [85, 71, 64, 59, 55, 61, 65, 83], 
        [87, 79, 69, 68, 65, 76, 78, 94]
      ]           

for i in range(0,8):
     for j in range(0,8):
             arr[i][j] = (arr[i][j],arr[i][j],arr[i][j])

#decompress_to_jpeg(arr)
#parse_string_to_quantized_image("image_output.txt")

#shift_image(0,0,new)
#convertBoxDCT(0,0,new)
#quantize(0,0,new)
