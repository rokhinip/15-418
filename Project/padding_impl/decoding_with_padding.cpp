#include <string>
#include <stdio.h>
#include <assert.h>
#include <iostream>
#include "../huffman.h"

void decode_chars(Encoding *encoding, char *chars_in_msg, 
                  int word_length, int num_words_in_msg){
  
  Rev_symbol_map *rev_symbol_map = build_reverse_symbol_map(encoding);
  string encoded_string = encoding->encoded_message;
  
  #pragma omp parallel for 
  for (int i = 0; i < num_words_in_msg; i++){
    string prefix = encoded_string.substr(i*word_length, word_length);
    chars_in_msg[i] = (*rev_symbol_map)[prefix];
  }

  return;
}


string huffman_decode(Encoding *encoding){

  Symbol_map symbol_map = encoding->symbol_map;
  Symbol_map::iterator it;
  int word_length = 0;
  for (it = symbol_map.begin(); it != symbol_map.end(); ++it) {

    if (word_length == 0) word_length = (it->second).length();
    // This checks to make sure that all the encodings are of the same length 
    assert((int) (it->second).length() == word_length);
  }
 
  if (word_length == 0) return "";
  int num_words_in_msg = (encoding->encoded_message).length() / word_length;
  
  char *chars_in_msg = new char[num_words_in_msg];
  decode_chars(encoding, chars_in_msg, word_length, num_words_in_msg);

  string decoded_msg = "";
  for (int i = 0; i < num_words_in_msg; i++){
    decoded_msg += chars_in_msg[i];
  }

  return decoded_msg;
}

