#include <string>
#include <iostream>
#include <queue>
#include <list>
#include <stdio.h>
#include <string.h>

#include "../huffman.h"

#define NUM_CHARS 256

using namespace std;

/**************************** Huffman Tree definition *************************/
typedef struct Huffman_tree_node {
    
  // All Nodes except Leaves have a '\0'
  char value;
  // Sum of Frequencies of that subtree 
  int frequency;
  // Left Subtree
  struct Huffman_tree_node *left;
  // Right Subtree
  struct Huffman_tree_node *right;
 
} Huffman_tree_node;

/*
 * Comparator for the Priority Queue.
 * Compares two Tree Nodes and returns true
 * if left has lower frequency than the right one 
 */
struct Huffman_tree_node_comparator {
  bool operator() (Huffman_tree_node *left, Huffman_tree_node *right){
    return left->frequency >  right->frequency;
  }
};

typedef priority_queue <Huffman_tree_node *, vector <Huffman_tree_node *>, 
Huffman_tree_node_comparator> Huffman_tree_queue;

/****************************** Util Functions ********************************/

// Merges the Left and Right Subtree Node
Huffman_tree_node* merge(Huffman_tree_node *left, Huffman_tree_node *right){
    
    // Construct the Tree Node for this merged Node
    Huffman_tree_node *merged_tree_node = new Huffman_tree_node();

    // Non Leaf Nodes have '\0' value
    merged_tree_node->value = (char)'\0';
    
    // Frequency is sum of subtrees 
    merged_tree_node->frequency = left->frequency + right->frequency;
    
    merged_tree_node->left = left;
    merged_tree_node->right = right;

    return merged_tree_node;
}

// In order traversal of the Huffman Tree to populate the symbol map
void populate_symbol_map(Huffman_tree_node *tree, string current, 
                         Encoding *encoding){
   
   if (tree == NULL) return;
   if (tree->left == NULL && tree->right == NULL){ // leaf node
   	encoding->symbol_map[tree->value] = current;
	return;
   } 	

   populate_symbol_map(tree->left, current + "0", encoding);
   populate_symbol_map(tree->right, current + "1", encoding);
   return;
}

//TODO: This can be done in parallel 
void build_frequency_array(char *message, int length, int *char_freq_array){
  
  for (int i = 0; i < length; i++){
    char_freq_array[ (int) message[i]]++;
  }

  return;
}

Huffman_tree_node *build_huffman_tree(int *char_freq_array){
  
  Huffman_tree_queue queue; 

  // Constructing Huffman Tree Leaves for chars of non-zero frequency
  for(int i = 0; i < NUM_CHARS; i++){
     if (char_freq_array[i] > 0){

         Huffman_tree_node *tree_node = new Huffman_tree_node();
         
         // 0 <= 0 < 256 and so (char) i has the same value as i
         tree_node->value = (char) i; 
    
         tree_node->frequency = char_freq_array[i];
      
         // Left and Right Pointers are NULL for the Leaves
         tree_node->left = NULL;
         tree_node->right = NULL;
 
         queue.push(tree_node);      	  
     }
  }
 
  if (queue.size() == 0) return NULL;
   
  // While queue is of size greater than 1, merge the least two elements
  while(queue.size() > 1){
      
    Huffman_tree_node *min = queue.top();
    queue.pop();

    Huffman_tree_node *second_min = queue.top();
    queue.pop();

    queue.push(merge(min, second_min));   
 
  }
  
  // Huffman_tree formed
  Huffman_tree_node* huffman_tree = queue.top();
  return huffman_tree; 
}

void delete_huffman_tree(Huffman_tree_node *huffman_tree){

  if (huffman_tree == NULL) return;
  delete_huffman_tree(huffman_tree->left);
  delete_huffman_tree(huffman_tree->right);

  delete huffman_tree;
  return;

}

void pad_encoding_symbol(Encoding *encoding){
 
  Symbol_map symbol_map = encoding->symbol_map; 

  int max_encoding_length = 0;
  Symbol_map::iterator it;
  for (it = symbol_map.begin(); it != symbol_map.end(); ++it){
    int encoding_length = (it->second).length();
    if (encoding_length > max_encoding_length)
      max_encoding_length = encoding_length;
  }

  for (it = symbol_map.begin(); it != symbol_map.end(); ++it){
    string value = it->second;
    string padding (max_encoding_length - (int) value.length(), '0');
    it->second = value + padding;
  }
  
  encoding->symbol_map = symbol_map; 
  return;
}


void build_encoded_string(char *message, int length, Encoding *encoding){

  string encoded_message = encoding->encoded_message;
  
  for (int i = 0; i < length; i++){
    encoded_message += (encoding->symbol_map)[message[i]];
  }
  
  encoding->encoded_message = encoded_message;
  return;
}


Encoding *huffman_encode(char *message, int length){

  int char_freq_array[NUM_CHARS] = {0};  
  
  build_frequency_array(message, length, char_freq_array); 
  Huffman_tree_node *huffman_tree = build_huffman_tree(char_freq_array);

  Encoding *encoding = new Encoding();
  populate_symbol_map(huffman_tree, "", encoding);
  pad_encoding_symbol(encoding);
  build_encoded_string(message, length, encoding);

  delete_huffman_tree(huffman_tree);
  return encoding;
}
